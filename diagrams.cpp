#include "diagrams.h"
#include "ui_diagrams.h"

#include <QtWidgets>

//#define ADD_DEPTH // добавление работы с глубиной
//#define TEST_OSC  // добавление тестовых осцилограмм
#define DTAXIS    // отображение основной оси в читабельном формате времени

//--------------------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------------------------
AxisTagD::AxisTagD(QCPAxis *parentAxis, QString objectName, QCPItemTracer::TracerStyle style, bool onlyTracer) :
    QObject(parentAxis),
    mAxis(parentAxis)
{
    isVisible = false;
    position = 0.0;

    this->setObjectName(objectName);

    mDummyTracer = new QCPItemTracer(mAxis->parentPlot());
    mDummyTracer->setVisible(false);
    mDummyTracer->setSelectable(false);
    mDummyTracer->position->setAxisRect(mAxis->axisRect());
    mDummyTracer->setStyle(style);
    mDummyTracer->setClipAxisRect(mAxis->axisRect());

    if (mAxis->orientation() == Qt::Horizontal)
    {
        mDummyTracer->position->setTypeX(QCPItemPosition::ptPlotCoords);
        mDummyTracer->position->setTypeY(QCPItemPosition::ptAxisRectRatio);
        mDummyTracer->position->setAxes(mAxis, nullptr);
    }
    else
    {
        mDummyTracer->position->setTypeX(QCPItemPosition::ptAxisRectRatio);
        mDummyTracer->position->setTypeY(QCPItemPosition::ptPlotCoords);
        mDummyTracer->position->setAxes(nullptr, mAxis);
    }

    if (!onlyTracer)
    {
        mArrow = new QCPItemLine(mAxis->parentPlot());
        mArrow->setVisible(false);
        mArrow->setSelectable(false);
        mArrow->setLayer("overlay");
        mArrow->setClipToAxisRect(false);
//        mArrow->setPen(QColor(Qt::lightGray));
        mArrow->setHead(QCPLineEnding::esNone);
        mArrow->end->setParentAnchor(mDummyTracer->position);
        mArrow->start->setParentAnchor(mArrow->end);
        if (mAxis->orientation() == Qt::Horizontal)
            mArrow->start->setCoords(0, -4);
        else
            mArrow->start->setCoords(-4, 0);

        mLabel = new QCPItemText(mAxis->parentPlot());
        mLabel->setVisible(false);
        mLabel->setSelectable(false);
        mLabel->setText("");
        mLabel->setLayer("overlay");
        mLabel->setClipToAxisRect(false);
        mLabel->setPadding(QMargins(2, 0, 1, 0));
        mLabel->setBrush(QBrush(Qt::lightGray));
        if (mAxis->orientation() == Qt::Horizontal)
            mLabel->setPositionAlignment(Qt::AlignLeft | Qt::AlignBottom);
        else
            mLabel->setPositionAlignment(Qt::AlignRight | Qt::AlignVCenter);
        mLabel->position->setParentAnchor(mArrow->start);
    }
}

//--------------------------------------------------------------------------------------------------------------------------------------
AxisTagD::~AxisTagD()
{
    if (mDummyTracer)
        mDummyTracer->parentPlot()->removeItem(mDummyTracer);
    if (mArrow)
        mArrow->parentPlot()->removeItem(mArrow);
    if (mLabel)
        mLabel->parentPlot()->removeItem(mLabel);
}

//--------------------------------------------------------------------------------------------------------------------------------------
void AxisTagD::setBrush(const QBrush &brush)
{
    if (mLabel)
        mLabel->setBrush(brush);
}

//--------------------------------------------------------------------------------------------------------------------------------------
void AxisTagD::setText(const QString &text)
{
    if (mLabel)
        mLabel->setText(text);
}

//--------------------------------------------------------------------------------------------------------------------------------------
void AxisTagD::setVisible(bool visible)
{
    isVisible = visible;

    if (mDummyTracer)
        mDummyTracer->setVisible(visible);
    if (mArrow)
        mArrow->setVisible(visible);
    if (mLabel)
        mLabel->setVisible(visible);
}

//--------------------------------------------------------------------------------------------------------------------------------------
void AxisTagD::setPosition(double value)
{
    position = value;

    if (mAxis->orientation() == Qt::Horizontal)
    {
        if (mAxis->scaleType() == QCPAxis::stLogarithmic && value < 0)
        {
            if (mDummyTracer)
                mDummyTracer->position->setCoords(mAxis->range().lower, 0);
        }
        else
        {
            if (mDummyTracer)
                mDummyTracer->position->setCoords(value, 0);
        }
        if (mArrow)
            mArrow->end->setCoords(0, -mAxis->offset());
    }
    else
    {
        if (mAxis->scaleType() == QCPAxis::stLogarithmic && value < 0)
        {
            if (mDummyTracer)
                mDummyTracer->position->setCoords(0, mAxis->range().lower);
        }
        else
        {
            if (mDummyTracer)
                mDummyTracer->position->setCoords(0, value);
        }
        if (mArrow)
            mArrow->end->setCoords(-mAxis->offset(), 0);
    }
}

//--------------------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------------------------------
static const auto defKeyTimeFormat = "dd.MM.yyyy\nhh:mm:ss.zzz";
static const auto defCsvTimeFormat = "dd.MM.yyyy hh:mm:ss";
static const auto defLasTimeFormat = "yyyyMMddhhmmss";

static QString timeFormatExample =  "yyyy-MM-dd hh:mm:ss";
static QString userTimeShift     = "+0000-00-00 00:00:00";
static QString userTimeFormat    = defCsvTimeFormat;

static const auto timeList = QStringList()
     << userTimeFormat

     << "yyyy MM dd hh:mm:ss"
     << "yyyy-MM-dd hh:mm:ss"
     << "yyyy.MM.dd hh:mm:ss"
     << "yyyy/MM/dd hh:mm:ss"
     << "yy MM dd hh:mm:ss"
     << "yy-MM-dd hh:mm:ss"
     << "yy.MM.dd hh:mm:ss"
     << "yy/MM/dd hh:mm:ss"
     << "yyyyMMddhhmmss"
     << "yyMMddhhmmss"

     << "dd MM yyyy hh:mm:ss"
     << "dd-MM-yyyy hh:mm:ss"
     << "dd.MM.yyyy hh:mm:ss"
     << "dd/MM/yyyy hh:mm:ss"
     << "dd MM yy hh:mm:ss"
     << "dd-MM-yy hh:mm:ss"
     << "dd.MM.yy hh:mm:ss"
     << "dd/MM/yy hh:mm:ss"
     << "ddMMyyyyhhmmss"
     << "ddMMyyhhmmss"

     << "ddd dd MM yyyy hh:mm:ss"
     << "ddd dd MM yyyy"
     << "ddd dd MM yy hh:mm:ss"
     << "ddd dd MM yy"

     << "hh:mm:ss"
     << "h:mm:ss"
     << "hhmmss"
        ;

Diagrams::Diagrams(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Diagrams)
{
    wakeTime.start();
    ui->setupUi(this);

    ui->customPlot->plotLayout()->clear();
    ui->customPlot->setInteractions(  QCP::iSelectAxes
                                    | QCP::iSelectItems
                                    | QCP::iSelectOther
//                                    | QCP::iSelectLegend
//                                    | QCP::iSelectPlottables
//                                    | QCP::iRangeDrag
//                                    | QCP::iRangeZoom
//                                    | QCP::iMultiSelect
                                    );

    QCPmainLayout = ui->customPlot->plotLayout();
    QCPmainLayout->setColumnSpacing(0);
    QCPmainLayout->setRowSpacing(0);
    QCPsubLayout = new QCPLayoutGrid;
    QCPsubLayout->setColumnSpacing(0);
    QCPsubLayout->setRowSpacing(0);

    connect(ui->customPlot, SIGNAL(selectionChangedByUser()), this, SLOT(selectionChanged()));
    connect(ui->customPlot, SIGNAL(mousePress(QMouseEvent *)), this, SLOT(mousePress(QMouseEvent *)));
    connect(ui->customPlot, SIGNAL(mouseRelease(QMouseEvent *)), this, SLOT(mouseRelease(QMouseEvent *)));
    connect(ui->customPlot, SIGNAL(mouseMove(QMouseEvent *)), this, SLOT(mouseMove(QMouseEvent *)));
    connect(ui->customPlot, SIGNAL(mouseWheel(QWheelEvent *)), this, SLOT(mouseWheel(QWheelEvent *)));
    connect(ui->bar, SIGNAL(valueChanged(int)), this, SLOT(scrollBarChanged(int)));

    ui->customPlot->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->customPlot, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(contextMenuRequest(QPoint)));

    clearAll();
//    parseMemFile();
}

//--------------------------------------------------------------------------------------------------------------------------------------
// отчистка диограмм
void Diagrams::clearAll()
{
    ui->customPlot->clearItems();
    ui->customPlot->clearGraphs();
    ui->customPlot->clearPlottables();
    ui->customPlot->clearMask();

    rectTagList.clear();
    graphTagList.clear();
    graphTracerList.clear();
    rectList.clear();
    rectWavList.clear();
    QCPmainLayout->clear();
    QCPsubLayout = new QCPLayoutGrid;
    QCPsubLayout->setColumnSpacing(0);
    QCPsubLayout->setRowSpacing(0);

    deselectAll();
    haveDepth = false;
}

//--------------------------------------------------------------------------------------------------------------------------------------
// отчистка диограмм
void Diagrams::deselectAll()
{
    foreach (auto graph,  ui->customPlot->selectedGraphs())
        graph->setSelection(QCPDataSelection());
    foreach (auto axis, ui->customPlot->selectedAxes())
        axis->setSelectedParts(QCPAxis::spNone);
    selectedAxis = nullptr;
}

//--------------------------------------------------------------------------------------------------------------------------------------
// обновление диаграмм (добовление меток расчет максимальной высоты)
void Diagrams::updateDiagrams()
{
    // выравниваем все планшеты по одному уровню (добавляю пустые оси)
    auto maxRectAxes = 0;
    foreach (auto rect, rectList)
        if (maxRectAxes < rect->axes(QCPAxis::atTop).size())
            maxRectAxes = rect->axes(QCPAxis::atTop).size();

    maxRange = QCPRange(0, 0);
    bool visible = false;

    auto range = mainAxis()->range();
    QCPRange newRange;

    foreach (auto rect, rectList)
    {
        foreach (auto graph, rect->graphs())
            visible |= graph->visible();

        if (visible)
        {
            auto rectAxesNum = rect->axes(QCPAxis::atTop).size();
            if (rectAxesNum < maxRectAxes)
                for (auto g=0; g<maxRectAxes-rectAxesNum; g++)
                    addGraph(rect);
        }

        auto axis = rect->axis(QCPAxis::atLeft);

        // добавляем метку для 1й оси и скрываем остальные оси
        rectTagList << new AxisTagD(axis, "rect", QCPItemTracer::tsCrosshair, rect != rectList.first());
        if (rect != rectList.first())
            axis->setTickLabels(false);

        // ищем и запоминаем максимальный размер кривых по всем планшетам
        auto newRange = getAxisFullRange(axis);
        if (maxRange.size() < newRange.size())
            maxRange = newRange;
    }

    scrollUpdate(maxRange, axisChanged(mainAxis()->property("AutoScale").toBool() ? maxRange : range));

    replot();
}

QCPRange Diagrams::getAxisFullRange(QCPAxis *axis)
{
    QCPRange range = axis->range();
    bool haveRange = false;
    foreach (auto p, axis->plottables())
    {
        auto signDomain = axis->scaleType() == QCPAxis::stLogarithmic ? (axis->range().upper < 0 ? QCP::sdNegative : QCP::sdPositive) : QCP::sdBoth;
        bool currentFoundRange;
        auto plottableRange = p->getKeyRange(currentFoundRange, signDomain);
        if (currentFoundRange)
        {
            if (!haveRange)
                range = plottableRange;
            else
                range.expand(plottableRange);
            haveRange = true;
        }
    }
    return range;
}

void Diagrams::axisUpdateRange(QCPAxis *axis)
{
//    axis->setScaleRatio(axis, 1.05);
}

//--------------------------------------------------------------------------------------------------------------------------------------
// обновление диограмм
void Diagrams::replot()
{
    // обновляю положение полосы прокрутки
    if (!rectList.isEmpty())
    {
#define EXTENDETVIEV
        auto shift = 0;
#ifdef EXTENDETVIEV
        shift = 16;
#endif
        ui->frameTop->setMinimumHeight(rectList.first()->top() - shift);
        ui->frameTop->setMaximumHeight(ui->frameTop->minimumHeight());
#ifndef EXTENDETVIEV
        ui->frameBot->setMinimumHeight(ui->customPlot->size().height() - mainAxis()->axisRect()->height() - ui->frameTop->minimumHeight());
        ui->frameBot->setMaximumHeight(ui->frameBot->minimumHeight());
#endif
    }

    // автомасштаб осей
    if (!selectedAxis)
    {
        foreach (auto element, QCPmainLayout->elements(true))
        {
            auto rect = dynamic_cast<QCPAxisRect *>(element);
            if (rect)
            {
                foreach (auto plottabl, rect->plottables())
                {
                    if (plottabl->valueAxis()->property("AutoScale").toBool())
                    {
                        auto range = getAxisFullRange(plottabl->valueAxis());
                        if (plottabl->valueAxis()->range() != range)
                        {
                            plottabl->rescaleValueAxis(false, true);
                            axisUpdateRange(plottabl->valueAxis());
                        }
                    }
                }
            }
        }
    }

    ui->customPlot->replot(QCustomPlot::rpQueuedReplot);
}

//--------------------------------------------------------------------------------------------------------------------------------------
// самая левая вертикальная ось - главная
QCPAxis *Diagrams::mainAxis()
{
    if (rectList.isEmpty())
        return nullptr;
    return rectList.first()->axis(QCPAxis::atLeft);
}

QPen Diagrams::setInvisibleAxis(QCPAxis *axis)
{
    auto pen = QPen(Qt::white);
    axis->setTickLabelColor(pen.color());
    axis->setLabelColor(pen.color());
    axis->setSelectedTickLabelColor(pen.color());
    axis->setSelectedLabelColor(pen.color());
    axis->setBasePen(pen);
    axis->setTickPen(pen);
    axis->setSubTickPen(pen);
    axis->setSelectedBasePen(pen);
    axis->setSelectedTickPen(pen);
    axis->setSelectedSubTickPen(pen);

    axis->setProperty("Invisible", true);
    axis->setProperty("AutoScale", false);
    axis->setRange(-1, 0);

    return pen;
}

//--------------------------------------------------------------------------------------------------------------------------------------
// добавление столбца для граффиков
QCPAxisRect *Diagrams::addRect(QCPLayoutGrid *layoutGrid, bool visible)
{
    if (!layoutGrid)
        layoutGrid = QCPmainLayout;

    auto rect = new QCPAxisRect(layoutGrid->parentPlot(), false);
    rect->setObjectName(QString("rect%1").arg(rectList.size()));
    auto axis = rect->addAxis(QCPAxis::atLeft);
    axis->setProperty("AutoScale", true);
#ifdef DTAXIS
    QSharedPointer<QCPAxisTickerDateTime> dateTimeTicker(new QCPAxisTickerDateTime);
    //dateTimeTicker->setDateTimeSpec(Qt::UTC);
    dateTimeTicker->setDateTimeFormat(defKeyTimeFormat);
    axis->setTicker(dateTimeTicker);
#endif
    axis->setRangeReversed(true);

    // добавляем столбец в конец или предпоследним если в конце осциллограммы
    auto col = QCPmainLayout->columnCount();
    if (layoutGrid->rowCount() && layoutGrid->element(0, QCPmainLayout->columnCount() - 1) == QCPsubLayout)
    {
        layoutGrid->insertColumn(QCPmainLayout->columnCount() - 1);
        col = QCPmainLayout->columnCount() - 2;
    }
    layoutGrid->addElement(0, col, rect);

    if (!visible)
    {
        setInvisibleAxis(axis);
        layoutGrid->setColumnStretchFactor(col, 0.1);
    }

    auto rAxis = rect->addAxis(QCPAxis::atRight);
#ifdef DTAXIS
    rAxis->setTicker(dateTimeTicker);
#endif
    rAxis->setRangeReversed(true);
    rAxis->setTickLabels(false);
    rAxis->grid()->setVisible(true);
    rAxis->grid()->setSubGridVisible(true);

    rectList << rect;
    return rect;
}

void Diagrams::clearGraph(const QString &name)
{
    auto graph = ui->customPlot->findChild<QCPGraph *>(name);
    if (!graph)
        return;
    graph->data().data()->clear();
    ui->customPlot->replot();
}

void Diagrams::clearAllGraph()
{
    for (auto i=0; i<ui->customPlot->graphCount(); i++)
        ui->customPlot->graph(i)->data().data()->clear();
    ui->customPlot->replot();
}

void Diagrams::addGraphPoints(const QString &name, const QVector<QCPGraphData>& data)
{
    auto graph = this->findChild<QCPGraph *>(name);
    if (!graph)
        return;
    if (!data.isEmpty())
        graph->data()->add(data);

    graph = this->findChild<QCPGraph *>("MainTime");
    if (!graph)
        return;
    if (!data.isEmpty())
        graph->data()->add(data);

    // обновление
    updateDiagrams();
    repaint();
}

//--------------------------------------------------------------------------------------------------------------------------------------
// облавляем график на столбец диограмм
QCPGraph *Diagrams::addGraph(QCPAxisRect *rect, const QVector<QCPGraphData>& data, const QString& name, const QColor& color, bool visible, bool last)
{
    auto pen = QPen(color.isValid() ? color : QColor(rand() % 245 + 10, rand() % 245 + 10, rand() % 245 + 10));
    QCPAxis *axis;
    if (last)
    {
        axis = rect->axes(QCPAxis::atTop).last();
        pen = QPen(Qt::white);
    }
    else
    {
        axis = rect->addAxis(QCPAxis::atTop);
        axis->setProperty("AutoScale", true);
        axis->setTickLabelColor(pen.color());
    }

    if (!visible)
        pen = setInvisibleAxis(axis);

    if (!last)
    {
        axis->grid()->setVisible(visible);
        axis->setLabel(name);
    }

    rect->setRangeDragAxes(nullptr, rect->axis(QCPAxis::atLeft));
    rect->setRangeZoomAxes(nullptr, rect->axis(QCPAxis::atLeft));

    auto graph = rect->parentPlot()->addGraph(rect->axis(QCPAxis::atLeft), axis);
    graph->setObjectName(name);
    graph->setName(name);
    graph->setProperty("Visible", visible);
    graph->setAdaptiveSampling(true);
    graph->data()->set(data);
    graph->setPen(pen);
    graph->valueAxis()->setScaleType(QCPAxis::stLinear);
    if (graph->keyAxis()->property("AutoScale").toBool())
        graph->rescaleKeyAxis();
    if (graph->valueAxis()->property("AutoScale").toBool())
        graph->rescaleValueAxis(false, true);

    graphTagList << new AxisTagD(axis, name);

    auto tracer = new QCPItemTracer(rect->parentPlot());
    tracer->setObjectName(name);
    tracer->setVisible(false);
    tracer->setSelectable(false);
    tracer->setGraph(graph);
    tracer->position->setAxisRect(rect);
    tracer->setClipAxisRect(rect);
    tracer->setStyle(QCPItemTracer::tsCircle);
    tracer->setPen(pen);
    tracer->setSize(5);
    graphTracerList << tracer;

    return graph;
}

//--------------------------------------------------------------------------------------------------------------------------------------
// добавляем осциллограмму
QCPGraph *Diagrams::addGraphWav(const QString& name, const QString& nameX, const QString& nameY, const QColor& color)
{
    // проверка были уже добавлены осциллограммы или нет
    bool hasSubLayout = false;
    foreach (auto element, QCPmainLayout->elements(true))
        if (element == QCPsubLayout)
            hasSubLayout = true;

    // если небыло добавляем вконце справа
    if (!hasSubLayout)
        QCPmainLayout->addElement(0, QCPmainLayout->columnCount(), QCPsubLayout);

    auto rect = new QCPAxisRect(QCPsubLayout->parentPlot(), false);
    rect->setObjectName(QString("rectWav%1").arg(rectWavList.size()));
    QCPsubLayout->addElement(QCPsubLayout->rowCount(), 0, rect);

    rect->addAxes(QCPAxis::atBottom | QCPAxis::atLeft);

    rect->axis(QCPAxis::atBottom)->grid()->setVisible(true);
    rect->axis(QCPAxis::atLeft)  ->grid()->setVisible(true);

    rect->axis(QCPAxis::atBottom)->setProperty("AutoScale", true);
    rect->axis(QCPAxis::atLeft)  ->setProperty("AutoScale", true);

//    rect->axis(QCPAxis::atBottom)->ticker()->setTickCount(16);
    rect->axis(QCPAxis::atBottom)->setLabel(nameX);
    rect->axis(QCPAxis::atLeft)  ->setLabel(nameY);

    auto graph = rect->parentPlot()->addGraph(rect->axis(QCPAxis::atBottom), rect->axis(QCPAxis::atLeft));
    graph->setObjectName(name);
    graph->setName(name);
    graph->setAdaptiveSampling(true);
    graph->rescaleAxes();
    graph->setPen(color.isValid() ? color : QColor(rand() % 245 + 10, rand() % 245 + 10, rand() % 245 + 10));

    rectWavList << rect;
    return graph;
}

//--------------------------------------------------------------------------------------------------------------------------------------
// обновление пределов осей
void Diagrams::rescaleAxis(QCPAxis *axis)
{
    auto range = getAxisFullRange(axis);
    if (axis->range() != range && range.size())
        axis->rescale(true);
}

//--------------------------------------------------------------------------------------------------------------------------------------
// перерасчет пределов осей
QCPRange Diagrams::axisChanged(const QCPRange &newRange)
{
    static const auto minRange = 10;
    auto lower = newRange.lower;
    auto upper = newRange.upper;

    if (newRange.lower < maxRange.lower)
    {
        lower = maxRange.lower;
        if (upper - lower < minRange)
            upper = lower + minRange;
    }
    if (newRange.upper > maxRange.upper)
    {
        upper = maxRange.upper;
        if (upper - lower < minRange)
            lower = upper - minRange;
    }

    // если меньше минимального оставляем минимальный
    if (upper - lower < minRange)
    {
        upper = lower + minRange;
    }

    // если больше максимального оставляем максимальный
    if (upper - lower > maxRange.size())
    {
        lower = maxRange.lower;
        upper = maxRange.upper;
    }

    auto range = QCPRange(lower, upper);
    foreach (auto rect, rectList)
    {
        rect->axis(QCPAxis::atLeft)->setRange(range);
        rect->axis(QCPAxis::atRight)->setRange(range);
    }
    return range;
}

//--------------------------------------------------------------------------------------------------------------------------------------
// обновление положения скролла
void Diagrams::scrollUpdate(QCPRange maxRange, QCPRange range)
{
    auto enable = maxRange != range;
    ui->bar->setEnabled(enable);
    if (!enable)
        return;
    ui->bar->setRange(qRound(maxRange.lower), qRound(maxRange.upper - range.size()));
//    ui->bar->setValue(qRound(range.lower));
    ui->bar->setPageStep(qRound(range.size()));
    ui->bar->setSingleStep(qRound(range.size() / 8));
}

//--------------------------------------------------------------------------------------------------------------------------------------
// слот для обработки пользовательского изменения скролла
void Diagrams::scrollBarChanged(int value)
{
    auto range = mainAxis()->range();
    range.upper = value + range.size();
    range.lower = value;
    axisChanged(range);
    replot();
}

//--------------------------------------------------------------------------------------------------------------------------------------
// обработка кручения колеса мышки
void Diagrams::mouseWheel(QWheelEvent *event)
{
    auto range = QCPRange(0, 0);
    foreach (auto rect, QList<QCPAxisRect *>() << rectList << rectWavList)
    {
        // проверяем находимся мы над диограммами или нет
        if (rect->outerRect().intersects(QRect(event->position().toPoint(), event->position().toPoint()))
            )
        {
            if (ui->customPlot->selectedAxes().isEmpty())
                deselectAll();
            if (selectedAxis && rect->axes().contains(selectedAxis))
            {
                auto axis = ui->customPlot->selectedAxes().first();
                const double wheelSteps = event->delta() / 120.0;
                const double factor = qPow(rect->rangeZoomFactor(axis->orientation()), wheelSteps);
                axis->scaleRange(factor, axis->pixelToCoord(axis->orientation() == Qt::Horizontal ? event->position().toPoint().x() : event->position().toPoint().y()));
                range = axis->range();
                axis->setRange(range);
                ui->customPlot->replot(QCustomPlot::rpQueuedReplot);
                return;
            }
            if (!rectList.contains(rect))
                continue;
            auto axis = rect->axis(QCPAxis::atLeft);
            const double wheelSteps = event->delta() / 120.0;
            const double factor = qPow(rect->rangeZoomFactor(axis->orientation()), wheelSteps);
            axis->scaleRange(factor, axis->pixelToCoord(axis->orientation() == Qt::Horizontal ? event->position().toPoint().x() : event->position().toPoint().y()));
            range = axis->range();
            break;
        }
    }
    if (range.size() > 0)
    {
        scrollUpdate(maxRange, axisChanged(range));
        replot();
    }
}

//--------------------------------------------------------------------------------------------------------------------------------------
// обработка движения мыши
void Diagrams::mouseMove(QMouseEvent *event)
{
/*
    if (!timerMouseMove)
    {
        timerMouseMove = new QElapsedTimer;
        timerMouseMove->start();
    }
    else
        if (timerMouseMove->elapsed() > 500)
            timerMouseMove->restart();
        else
            return;
*/
    // мышка над граффиками?
    auto moveSkip = true;
    foreach (auto rect, rectList)
        if (rect->rect().intersects(QRect(event->pos(), event->pos())))
            moveSkip = false;
    if (moveSkip)
        return;
/*
    // показ меток со значениями
    if (event->buttons().testFlag(Qt::RightButton))
    {
        double key = 0.0;
        if (auto graph = ui->customPlot->findChild<QCPGraph *>("MainTime"))
        {
            if (auto tracer = ui->customPlot->findChild<QCPItemTracer *>(graph->name()))
            {
                tracer->setGraphKey(graph->keyAxis()->pixelToCoord(event->pos().y()));
                key = tracer->position->coords().x();
            }
        }

        // обновление горизонтальных меток
        foreach (auto rect, rectList)
        {
            foreach (auto graph, rect->graphs())
            {
                if (graph->data()->isEmpty())
                    continue;

                if (auto tracer = ui->customPlot->findChild<QCPItemTracer *>(graph->name()))
                {
                    tracer->setGraphKey(graph->keyAxis()->pixelToCoord(event->pos().y()));
                    tracer->setVisible(true);
                    double value = tracer->position->coords().y();

                    if (auto tag = ui->customPlot->findChild<AxisTagD *>(graph->name()))
                    {
                        tag->setText(QString::number(value));
                        // Показываем значение только в пределах расположения графика
                        if (value > graph->valueAxis()->range().upper)
                            value = graph->valueAxis()->range().upper;
                        if (value < graph->valueAxis()->range().lower)
                            value = graph->valueAxis()->range().lower;
                        tag->setPosition(value);
                        tag->setVisible(graph->property("Visible").toBool());
                    }

                    // обновление осцилограмм
                    auto name = graph->name();
                    if (name.contains("Osc"))
                    {
                        name = name.split("_").last();
                        if (auto oscGraph = ui->customPlot->findChild<QCPGraph *>(name))
                        {
                            auto osc = oscMap.value(oscGraph->name());
                            double oscKey = tracer->position->coords().x();
                            auto data = osc->value(oscKey);
                            if (!data.isEmpty())
                            {
                                oscGraph->data()->set(data);
                                if (oscGraph->keyAxis()->property("AutoScale").toBool())
                                    oscGraph->rescaleKeyAxis();
                                if (oscGraph->valueAxis()->property("AutoScale").toBool())
                                    oscGraph->rescaleValueAxis(false, true);
                            }
                        }
                    }
                }
            }
        }
        // обновление вертикальных метки
        foreach (auto tag, rectTagList)
        {
#ifdef DTAXIS
            auto keyStr = QDateTime::fromMSecsSinceEpoch(static_cast<qint64>(key * 1000)).toString(defKeyTimeFormat);
            tag->setText(keyStr);
#else
            tag->setText(QString::number(key));
#endif
            tag->setPosition(key);
            tag->setVisible(true);
        }
        replot();//ui->customPlot->replot(QCustomPlot::rpQueuedReplot);
    }
*/
    if (selectedAxis)
        return;

    // двигаем графики
    if (event->buttons().testFlag(Qt::LeftButton))
    {
        auto diff = event->pos().y() - mMousePressPos.y();
        if (qAbs(diff) > 1 && maxRange != mainAxis()->range())
            ui->bar->setValue(static_cast<int>(ui->bar->value() + (diff < 0 ? 1 : -1) * ui->bar->singleStep() / 2));
        replot();
    }
    mMousePressPos = event->pos();
}

//--------------------------------------------------------------------------------------------------------------------------------------
// обработка нажатия кнопки мышки
void Diagrams::mousePress(QMouseEvent *event)
{
    buttonsPressed = event->buttons();
    if (event->buttons().testFlag(Qt::LeftButton))
    {
        deselectAll();
        ui->customPlot->replot(QCustomPlot::rpQueuedReplot);
    }
}

//--------------------------------------------------------------------------------------------------------------------------------------
// обработка отпускания кнопки мышки
void Diagrams::mouseRelease(QMouseEvent *event)
{
    // если нажата левая клавиша с правой и правую отпустили
    if (
               buttonsPressed.testFlag(Qt::LeftButton)
            && buttonsPressed.testFlag(Qt::RightButton)
            && event->buttons().testFlag(Qt::RightButton)
            )
    {
        auto startTag = ui->customPlot->findChild<AxisTagD *>("StartTag");
        auto stopTag = ui->customPlot->findChild<AxisTagD *>("StopTag");
        if (startTag && stopTag && haveDepth)
        {
            if (!(startTag->isVisible ^ stopTag->isVisible))
            {
                stopTag->setVisible(false);
                startTag->setText("Start\n" + rectTagList.first()->mLabel->text());
                startTag->setPosition(rectTagList.first()->position);
                startTag->setVisible(true);
                startTag->mLabel->setLayer("axes");
            }
            else if (startTag->isVisible && !stopTag->isVisible)
            {
                stopTag->setText("Stop\n" + rectTagList.first()->mLabel->text());
                stopTag->setPosition(rectTagList.first()->position);
                stopTag->setVisible(true);
                stopTag->mLabel->setLayer("axes");
            }
        }
    }

    // если кликнули только левой клавишей
    if (
               buttonsPressed.testFlag(Qt::LeftButton)
            && !buttonsPressed.testFlag(Qt::RightButton)
            && event->buttons().testFlag(Qt::NoButton)
            )
    {
        if (auto tag = ui->customPlot->findChild<AxisTagD *>("StartTag"))
            tag->setVisible(false);
        if (auto tag = ui->customPlot->findChild<AxisTagD *>("StopTag"))
            tag->setVisible(false);
    }

    // отчищаем при отпускании всех
    foreach (auto tag, QList<AxisTagD *>() << rectTagList << graphTagList)
        tag->setVisible(false);
    foreach (auto tracer, graphTracerList)
        tracer->setVisible(false);

    ui->customPlot->replot(QCustomPlot::rpQueuedReplot);
}

//--------------------------------------------------------------------------------------------------------------------------------------
// выделение графиков
void Diagrams::selectionChanged()
{
    foreach (auto axis, ui->customPlot->selectedAxes())
    {
        if (selectedAxis != axis
                && ((axis->orientation() == Qt::Horizontal && rectList.contains(axis->axisRect()))
                    || rectWavList.contains(axis->axisRect())
                    )
                )
        {
            selectedAxis = axis;
            selectedAxis->setSelectedParts(QCPAxis::spAxis | QCPAxis::spTickLabels | QCPAxis::spAxisLabel);
            foreach (auto graph, axis->graphs())
                graph->setSelection(QCPDataSelection(graph->data().data()->dataRange()));
        }
        else
        {
            if (selectedAxis)
                deselectAll();
            if (axis->orientation() == Qt::Vertical && rectList.contains(axis->axisRect()))
                axis->setSelectedParts(QCPAxis::spNone);
        }
    }
}

void Diagrams::setAutoScale(QCPAxis *axis, bool checked)
{
    axis->setProperty("AutoScale", checked);
    auto font = axis->labelFont();
    font.setBold(!checked);
    axis->setLabelFont(font);
    font = axis->tickLabelFont();
    font.setBold(!checked);
    axis->setTickLabelFont(font);
    if (!checked)
    {
        foreach (auto axis, ui->customPlot->selectedAxes())
            axis->setSelectedParts(QCPAxis::spNone);
        selectedAxis = axis;
        selectedAxis->setSelectedParts(QCPAxis::spAxis | QCPAxis::spTickLabels | QCPAxis::spAxisLabel);

        // если отключили автомасштаб и есть дефолтный диапазон то ставим его
        auto var = axis->property("Range");
        if (var.isValid())
            axis->setRange(var.toList().first().toDouble(), var.toList().last().toDouble());
    }
    else
    {
        rescaleAxis(axis);
        axisUpdateRange(axis);
    }
    ui->customPlot->replot(QCustomPlot::rpQueuedReplot);
}

QList<QAction *> Diagrams::newAxisActions(QCPAxis *axis)
{
    QList<QAction *> actions;

    if (axis->property("Invisible").toBool())
        return actions;

    auto actionAutoScale = new QAction("AutoScale");
    actions << actionAutoScale;
    actionAutoScale->setCheckable(true);
    actionAutoScale->setChecked(axis->property("AutoScale").toBool());
    connect(actionAutoScale, &QAction::triggered, this, [=] ()
    {
        setAutoScale(axis, actionAutoScale->isChecked());
    });

    auto actionRange = new QAction("Set custom range...");
    actions << actionRange;
    connect(actionRange, &QAction::triggered, this, [=] ()
    {
        auto dialog = new QDialog();
        dialog->setWindowTitle("Axis custom range");

        auto min = new QDoubleSpinBox();
        min->setPrefix("Min: ");
        min->setRange(-DBL_MAX, DBL_MAX);
        min->setValue(axis->range().lower);

        auto max = new QDoubleSpinBox();
        max->setPrefix("Max: ");
        max->setRange(-DBL_MAX, DBL_MAX);
        max->setValue(axis->range().upper);

        auto buttons = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
        connect(buttons, SIGNAL(accepted()), dialog, SLOT(accept()));
        connect(buttons, SIGNAL(rejected()), dialog, SLOT(reject()));

        auto mainLayout = new QVBoxLayout;
        mainLayout->addWidget(min);
        mainLayout->addWidget(max);
        mainLayout->addWidget(buttons);
        dialog->setLayout(mainLayout);
        dialog->setModal(true);
        if (dialog->exec() == QDialog::Rejected)
            return;

        axis->setRange(min->value(), max->value());

        if (actionAutoScale->isChecked())
            actionAutoScale->trigger();
        else
            ui->customPlot->replot(QCustomPlot::rpQueuedReplot);
    });

    auto actionLogarithmic = new QAction("Logarithmic format");
    actions << actionLogarithmic;
    actionLogarithmic->setCheckable(true);
    actionLogarithmic->setChecked(axis->scaleType() == QCPAxis::stLogarithmic);
    connect(actionLogarithmic, &QAction::triggered, this, [=] ()
    {
        if (axis->scaleType() == QCPAxis::stLinear)
        {
            setAxisScaleType(axis, QCPAxis::stLogarithmic);
        }
        else
            setAxisScaleType(axis, QCPAxis::stLinear);
        ui->customPlot->replot(QCustomPlot::rpQueuedReplot);
    });

    return actions;
}

void Diagrams::setAxisScaleType(QCPAxis *axis, QCPAxis::ScaleType type)
{
qDebug()
        << axis->numberFormat()
        << axis->numberPrecision()
        << axis->ticker()
        << axis->padding()
        << axis->tickLabelPadding()
            ;

    if (type == QCPAxis::stLogarithmic)
    {
        QSharedPointer<QCPAxisTickerLog> ticker(new QCPAxisTickerLog);
        axis->setTicker(ticker);
        axis->setNumberFormat("eb");
        axis->setNumberPrecision(0);
        axis->grid()->setSubGridVisible(true);
    }
    else
    {
        QSharedPointer<QCPAxisTicker> ticker(new QCPAxisTicker);
        axis->setTicker(ticker);
        axis->setNumberFormat("g");
        axis->setNumberPrecision(6);
        axis->grid()->setSubGridVisible(false);
    }
    axis->setRange(axis->range());
    axis->setScaleType(type);
}

//--------------------------------------------------------------------------------------------------------------------------------------
// контекстное меню
void Diagrams::contextMenuRequest(QPoint pos)
{
    // если не над графиками
    auto skip = false;
    foreach (auto rect, QList<QCPAxisRect *>() << rectList << rectWavList)
        if (rect->rect().intersects(QRect(pos, pos)))
            skip = true;
    if (skip)
        return;

    auto menu = new QMenu(this);
    menu->setAttribute(Qt::WA_DeleteOnClose);

    // если на одной из осей
    foreach (auto rect, ui->customPlot->axisRects())
        foreach (auto axis, rect->axes())
            if (axis->selectTest(pos, false) >= 0
                    && (
                        (axis->orientation()  == Qt::Horizontal && rectList.contains(axis->axisRect()))
                        || rectWavList.contains(axis->axisRect())
                        )
                    )
                menu->addActions(newAxisActions(axis));

    if (mainAxis()->selectTest(pos, false) >= 0)
    {
        auto actionAutoScale = new QAction("AutoScale");
        actionAutoScale->setCheckable(true);
        actionAutoScale->setChecked(mainAxis()->property("AutoScale").toBool());
        connect(actionAutoScale, &QAction::triggered, this, [=] ()
        {
            setAutoScale(mainAxis(), actionAutoScale->isChecked());
        });
        menu->addAction(actionAutoScale);
    }

    if (menu->actions().isEmpty())
    {

//        menu->addAction("Parse mem file...", this, &Diagrams::parseMemFile);
#ifdef ADD_DEPTH
        if (!rectList.isEmpty())
            menu->addAction("Add depth...", this, &Diagrams::addDepth);
#endif
        menu->addSeparator();

        if (!rectList.isEmpty())
        {
            menu->addAction("Screenshot...", this, SLOT(screenshotGraphs()));
            menu->addSeparator();

            menu->addAction("Export data...", this, &Diagrams::exportData);
            menu->addSeparator();
        }

//        menu->addAction("Generate test mem file...", this, &Diagrams::makeTestMemFile);

        menu->addAction("Clear", this, &Diagrams::clearAllGraph);
    }

    menu->popup(this->mapToGlobal(pos));
}

//--------------------------------------------------------------------------------------------------------------------------------------
void Diagrams::screenshotGraphs()
{
    QMessageBox msgBox("Save screenshot", "Choose save format screenshot", QMessageBox::NoIcon, QMessageBox::Cancel, QMessageBox::NoButton, QMessageBox::NoButton);
    auto jpgButton = msgBox.addButton("JPG (*.jpg)", QMessageBox::ActionRole);
    auto pngButton = msgBox.addButton("PNG (*.png)", QMessageBox::ActionRole);
    QApplication::clipboard()->setPixmap(ui->customPlot->toPixmap());

    msgBox.setDefaultButton(QMessageBox::Cancel);
    if (msgBox.exec() == QMessageBox::Cancel)
        return;

    auto name = this->property("fileName").toString().remove(".dat");
    QString fileName;
    auto ok = true;
    if (msgBox.clickedButton() == jpgButton)
    {
        fileName = QFileDialog::getSaveFileName(nullptr, "Save file", name, "JPG (*.jpg)");
        if (!fileName.isEmpty())
            ok = ui->customPlot->saveJpg(fileName);
    }
    else if (msgBox.clickedButton() == pngButton)
    {
        fileName = QFileDialog::getSaveFileName(nullptr, "Save file", name, "PNG (*.png)");
        if (!fileName.isEmpty())
            ok = ui->customPlot->saveBmp(fileName);
    }
    if (!ok)
        QMessageBox::critical(nullptr, "Error!", QString("Error save file:\n%1").arg(fileName), QMessageBox::Ok, QMessageBox::Ok);
}

//--------------------------------------------------------------------------------------------------------------------------------------
graphData Diagrams::makeGraphData(QVector<double> *valX, double valY, bool alreadySorted)
{
    if (!alreadySorted)
    {
        std::sort(valX->begin(), valX->end());
        valX->erase(std::unique(valX->begin(), valX->end() ), valX->end());
    }
    graphData time;
    time.reserve(valX->size());
    foreach (auto val, *valX)
        time.append(QCPGraphData(val, valY));
    time.squeeze();
    return time;
}
/*
//--------------------------------------------------------------------------------------------------------------------------------------
// распаковка файла с данными
void Diagrams::parseMemFile()
{
    static QString fileName;
    fileName = QFileDialog::getOpenFileName(nullptr, "Open file", fileName, "(*.dat, *.bin)");
    if (fileName.isEmpty())
    {
        QTimer::singleShot(10, this, &Diagrams::close);
        return;
    }

    this->setProperty("fileName", fileName);
    this->setWindowTitle(fileName);

    auto settings   = getMapfromJSON("settings/diagrams.json");
    auto paramsDesc = settings.value("PARAMS_DESCRIPT").toList();
    auto params     = settings.value("PARAMS").toMap();
    auto frames     = settings.value("FRAMES").toMap();
    auto diagrams   = settings.value("DIAGRAMS").toMap();

    if (settings.isEmpty() || paramsDesc.isEmpty() || params.isEmpty() || frames.isEmpty() || diagrams.isEmpty())
        return;

    auto pdType     = paramsDesc.indexOf("TYPE");
    auto pdDesc     = paramsDesc.indexOf("DESCRIPTION");
    auto pdUnit     = paramsDesc.indexOf("UNITS");
    auto pdColor    = paramsDesc.indexOf("COLOR");
    auto pdMin      = paramsDesc.indexOf("MIN");
    auto pdMax      = paramsDesc.indexOf("MAX");
    auto pdScale    = paramsDesc.indexOf("SCALE");

    auto check_crc         = settings.value("MAIN").toMap().value("CHECK_CRC").toBool();
    auto only_one_interval = settings.value("MAIN").toMap().value("ONLY_ONE_INTERVAL").toBool();

    // открываем файл
    QFile file(fileName);
    if(file.open(QIODevice::ReadOnly))
    {
        QProgressDialog progress("Load data...", "Cancel", 0, static_cast<int>(file.size()), this);
        progress.setWindowModality(Qt::ApplicationModal);
        progress.setValue(0);
        progress.show();

        //----------------------------------------------------------------------------------------------------------------------
        // создаем вектора для данных с резервированием размера вектора на основе размера файла
        auto paramDataSize = static_cast<int>(file.size()) / 100;

        wellTime.clear();
        wellTime.reserve(paramDataSize);

        //! сделать ключ из номера модуля и номера фрэйма
        auto makeFrameDataKey = [=] (auto devNum, auto frameNum)
        {
            return (devNum << 8) + frameNum;
        };

        //! получить номер модуля из ключа
        auto getDevNum = [=] (auto key)
        {
            return (key >> 8) & 0xFF;
        };

        //! получить номер фрэйма из ключа
        auto getFrameNum = [=] (auto key)
        {
            return key & 0xFF;
        };

        //! список параметров для фрэйма
        auto paramList = [=] (auto devNum, auto frameNum)
        {
            return frames.value(QString::number(devNum)).toMap().value(QString::number(frameNum));
        };

        QMap<int, QVector<graphData> *> framesData;
        foreach (auto devNum, frames.keys())
            foreach (auto frameNum, frames.value(devNum).toMap().keys())
                framesData.insert(makeFrameDataKey(devNum.toInt(), frameNum.toInt()), new QVector<graphData>(paramList(devNum.toInt(), frameNum.toInt()).toList().size()));

        //----------------------------------------------------------------------------------------------------------------------
        uint32_t lastTime = 0;
        double time;
        double add = 0;

        framesInfo framesInf;
        const auto timeMaxBackStep = 120;    // сек

        QElapsedTimer timer;
        timer.start();

        // парcим файл
        while (!file.atEnd())
        {
            if (!updateProbress(&progress, static_cast<int>(file.pos())))
                break;

            // читаем из файла
            QByteArray buff;
            auto ok = getFrame(&file, &buff, &framesInf);
            if (ok == FRAME_FILE_FALSE)
                break;
            else if (ok == FRAME_NEXT || ok == FRAME_ERR_SIZE || (ok == FRAME_ERR_CRC && check_crc))
                continue;

            frameHead *head = reinterpret_cast<frameHead *>(buff.data());
            char *ptr = buff.data() + sizeof(frameHead);

            if (only_one_interval && head->dt + timeMaxBackStep < lastTime)
                break;

            if (lastTime == head->dt)
                add += 1.0 / 10.0;
            else
                add = 0;

            lastTime = head->dt;
            time = lastTime + add;
            wellTime.append(time);

            auto frameParams = paramList(head->devType, head->frameType).toList();
            if (frameParams.isEmpty())
            {
toDebug(head, "Unknown frame!");
                framesInf.unknownFrame++;
            }
            else
            {
                QVector<graphData> *frame = framesData.value(makeFrameDataKey(head->devType, head->frameType), nullptr);
                if (frame)
                {
                    for (auto i=0; i<frameParams.size(); i++)
                    {
                        auto type = QMetaType::type(params.value(frameParams.at(i).toString()).toList().at(pdType).toByteArray().data());
                        (*frame)[i].append(QCPGraphData(time, static_cast<double>(QVariant(type, ptr).toReal())));
                        ptr += QMetaType::sizeOf(type);
                    }
                }
            }
        }
        auto parseInfo = QString("Распаковка завершена\n\n"
                                 "Найдено фреймов           \t\t %1\n"
                                 "Ошибок размера фреймов      \t %2\n"
                                 "Ошибок CRC                \t\t %3\n"
                                 "Неизвестных фреймов         \t %4\n"
                                 ).arg(framesInf.total).arg(framesInf.errorSize).arg(framesInf.errorCRC).arg(framesInf.unknownFrame);
        file.close();

        qDebug() << timer.elapsed();

        //----------------------------------------------------------------------------------------------------------------------
        // оптимизация векторов
        foreach (auto frame, framesData.values())
            for (auto i=0; i<frame->size(); i++)
                (*frame)[i].squeeze();

        wellTime.squeeze();
        mainTime = wellTime;

        //----------------------------------------------------------------------------------------------------------------------
        // тестовый вывод в текстовом виде так как все лежало в памяти
//        if (QApplication::keyboardModifiers().testFlag(Qt::ShiftModifier))
        {
            //! вывод параметров в файл
            auto saveToFie = [=] (auto file, auto progress, auto posShift, auto devNum, auto frameNum)
            {
                auto frame = framesData.value(makeFrameDataKey(devNum, frameNum));
                if (frame->isEmpty() || (*frame).first().isEmpty())
                    return;

                if(file->open(QIODevice::WriteOnly))
                {
                    QTextStream out(file);
                    out.setCodec(QTextCodec::codecForName("Windows-1251"));

                    auto frameParamList = paramList(devNum, frameNum).toStringList();

                    out << "Time;";
                    foreach (auto paramName, frameParamList)
                        out << params.value(paramName).toList().value(pdDesc).toString() + ", " + params.value(paramName).toList().value(pdUnit).toString() << ";";
                    out << "\n";

                    for (auto i=0; i<frame->first().size(); i++)
                    {
                        if (!updateProbress(progress, posShift + i))
                            break;

                        out << QDateTime::fromTime_t(static_cast<uint>(frame->first().at(i).key)).toString(defCsvTimeFormat) << ";";
                        for (auto j=0; j<frameParamList.size(); j++)
                            out << frame->at(j).at(i).value << ";";
                        out << "\n";
                    }
                }
                file->close();
            };

            auto size = 0;
            foreach (auto frame, framesData.values())
                size += (*frame).first().size();

            progress.setLabelText("Save data...");
            progress.setValue(0);
            progress.setMaximum(size);
            auto posShift = 0;

            foreach (auto key, framesData.keys())
            {
                auto devNum = getDevNum(key);
                auto frameNum = getFrameNum(key);
                posShift = progress.value();
                auto fName = QFileInfo(fileName).completeBaseName();
                auto fileDir = QFileInfo(fileName).path() + '/' + fName;
                QDir().mkdir(fileDir);
                file.setFileName(QString("%1/%2 dev(%3) frame(%4).csv").arg(fileDir).arg(fName).arg(devNum).arg(frameNum));
                saveToFie(&file, &progress, posShift, devNum, frameNum);
            }
        }

        progress.close();

        //----------------------------------------------------------------------------------------------------------------------
        //! добавление осциллограммы в колонку
        auto addGr = [=] (auto rect, auto val, auto paramName)
        {
            auto param = params.value(paramName).toList();

            auto grName = param.value(pdDesc).toString() + (param.value(pdUnit).toString().isEmpty() ? "" : ", ") + param.value(pdUnit).toString();
            auto gr = addGraph(rect, val, grName, param.value(pdColor).toString(), true);

            if (param.value(pdMin) != param.value(pdMax))
            {
                gr->valueAxis()->setProperty("Range", QVariantList() << param.value(pdMin) << param.value(pdMax));
                setAutoScale(gr->valueAxis(), false);
            }
            else
                setAutoScale(gr->valueAxis(), true);

            if (param.value(pdScale) == "log")
                setAxisScaleType(gr->valueAxis(), QCPAxis::stLogarithmic);
            else
                setAxisScaleType(gr->valueAxis(), QCPAxis::stLinear);
        };

        //! заполнение диаграмм
        auto fillDiagrams = [=] (auto name)
        {
            foreach (auto col, diagrams.value(name).toList())
            {
                auto rect = addRect(QCPmainLayout);
                foreach (auto grName, col.toList())
                {
                    auto devNum = -1;
                    auto frameNum = -1;
                    foreach (auto devNumber, frames.keys())
                        foreach (auto frameNumber, frames.value(devNumber).toMap().keys())
                            if (paramList(devNumber.toInt(), frameNumber.toInt()).toList().contains(grName))
                            {
                                devNum = devNumber.toInt();
                                frameNum = frameNumber.toInt();
                            }
                    if (devNum < 0 || frameNum < 0)
                        continue;

                    auto frame = framesData.value(makeFrameDataKey(devNum, frameNum));
                    auto frameParamList = paramList(devNum, frameNum).toStringList();
                    addGr(rect, frame->value(frameParamList.indexOf(grName.toString())), grName.toString());
                }
            }
        };

        //----------------------------------------------------------------------------------------------------------------------
        clearAll();

#ifdef ADD_DEPTH
        // добавляем глубину (пока что пустую)
        auto rect = addRect(QCPmainLayout);
        addGraph(rect, QVector<QCPGraphData>(), "DEPT, m",     Qt::darkGreen, true);
        addGraph(rect, QVector<QCPGraphData>(), "LAS DEPT, m", Qt::green,     true);
#endif
        haveDepth = false;

        // добавляем данные в диограммы
        QMessageBox msgBox(QMessageBox::NoIcon, "", parseInfo);
        foreach (auto diagram, diagrams.keys())
            msgBox.addButton(diagram, QMessageBox::AcceptRole);
        msgBox.exec();

        // добавляем графики в порядке описанном в файле настроек
        fillDiagrams(msgBox.clickedButton()->text());

        // добавляем нивидимое время для показ значени
        addGraph(rectList.first(), makeGraphData(&mainTime, 0, false), "MainTime", Qt::red, false, false);//true);

#ifdef TEST_OSC
        //добавляю тестовые осцилограммы
        {
            // получения случайного значение (отклонение на max вокруг val)
            auto getData = [] (float val, float max)
            {
                #define RAND_MAX 0x7fff
                float rnd = static_cast<float>(rand()) / static_cast<float>(RAND_MAX) * max - max / 2;
                float result = val + rnd;
                return result;
            };

            auto fillOsc = [=] (int val)
            {
                graphData osc;
                for (auto i=0; i<256; i++)
                    osc.append(QCPGraphData(i, double(getData(val, 500))));
                osc.squeeze();
                return osc;
            };

            // массив осцилограмм с ключем по времени
            oscData vecOscX;
            oscData vecOscY;
            oscData vecOscZ;

            // для добавление в планшет для невидимых времен осцилограмм
            QVector<double> vecOscVibX;
            QVector<double> vecOscVibY;
            QVector<double> vecOscVibZ;

            // заполняем рандомными значениями
            for (auto utc=mainTime.first(); utc<mainTime.last(); utc+=(mainTime.last()-mainTime.first())/20)
            {
                vecOscX.insert(utc, fillOsc(-250));
                vecOscY.insert(utc, fillOsc(0));
                vecOscZ.insert(utc, fillOsc(+250));

                vecOscVibX.append(utc);
                vecOscVibY.append(utc);
                vecOscVibZ.append(utc);
            }

            // добавляем осциллограммы
            addGraphWav("VibX", "Time, ms", "VibX, g")->valueAxis()->setProperty("Range", QVariantList() <<   -10.0 <<    10.0);
            addGraphWav("VibY", "Time, ms", "VibY, g")->valueAxis()->setProperty("Range", QVariantList() <<   -10.0 <<    10.0);
            addGraphWav("VibZ", "Time, ms", "VibZ, g")->valueAxis()->setProperty("Range", QVariantList() <<   -10.0 <<    10.0);

            oscMap.insert("VibX", new oscData(vecOscX));
            oscMap.insert("VibY", new oscData(vecOscY));
            oscMap.insert("VibZ", new oscData(vecOscZ));

            // добавляем на планшет для поиска временных меток
            addGraph(rectList.first(), makeGraphData(&vecOscVibX, 0, false),  "Osc_VibX",  Qt::red,      false,    true);
            addGraph(rectList.first(), makeGraphData(&vecOscVibY, 0, false),  "Osc_VibY",  Qt::red,      false,    true);
            addGraph(rectList.first(), makeGraphData(&vecOscVibZ, 0, false),  "Osc_VibZ",  Qt::red,      false,    true);
        }
#endif

        //----------------------------------------------------------------------------------------------------------------------
        // снимаем выделение
        foreach (auto axis, ui->customPlot->selectedAxes())
            axis->setSelectedParts(QCPAxis::spNone);

        // обновление
        updateDiagrams();
        repaint();

        // инициализация меток для экспорта
        auto startTag = new AxisTagD(rectTagList.first()->mAxis, "StartTag", QCPItemTracer::tsCrosshair);
        auto stopTag = new AxisTagD(rectTagList.first()->mAxis,  "StopTag",  QCPItemTracer::tsCrosshair);
        startTag->mLabel->setBrush(QBrush(Qt::cyan));
        stopTag->mLabel->setBrush(QBrush(Qt::cyan));
    }
    else
        QMessageBox::critical(nullptr, "Error!", QString("Error open file:\n%1").arg(fileName), QMessageBox::Ok, QMessageBox::Ok);
}
*/
void Diagrams::addDepth()
{
    static QString fileName;
    fileName = QFileDialog::getOpenFileName(nullptr, "Open file", fileName, "CSV file (*.csv)");
    if (!fileName.size())
        return;

    auto readListFromFile = [] (QFile *file)
    {
        auto line = QString::fromLocal8Bit(file->readLine());
        auto sep = ';';
        if (!line.contains(sep))
            sep = ',';

        QStringList list;
        foreach (auto text, line.simplified().split(sep, Qt::SkipEmptyParts))
        {
            text.remove("\"");
            list << text.simplified();
        }
        return list;
    };

    // открываем файл
    QFile file(fileName);
    if(file.open(QIODevice::ReadOnly))
    {
        //----------------------------------------------------------------------------------------------------------------
        // выбор колонок времени и даты
        auto list  = readListFromFile(&file);
        auto listExample = readListFromFile(&file);

        auto dialog = new QDialog;
        dialog->setMinimumWidth(600);
        dialog->setWindowTitle(QString("Select time and depth data in file: %1").arg(fileName));

        //----------------------------------------------------------------------------------------------------------------
        auto timeCombo = new QComboBox;
        timeCombo->addItems(list);
        timeCombo->setToolTip("Select column in file, where time data is located");
        static int timeComboIndex = 0;

        auto timeExample = new QLineEdit;
        timeExample->setReadOnly(true);
        timeExample->setEnabled(false);
        timeExample->setToolTip("Time column first value");

        qint64 *dtShift = new qint64;
        *dtShift = 0;

        auto timeShift = new QLineEditMy();
        timeShift->setInputMask("#0000-00-00 00:00:00");
        timeShift->setValidator(new QRegExpValidator(QRegExp("[+-]{1}([0-9]{4}-[0-9]{2}-[0-9]{2} [0-2]{1}[0-9]{1}:[0-5]{1}[0-9]{1}:[0-5]{1}[0-9]{1})")));
        timeShift->setText(userTimeShift);
        timeShift->setCursorPosition(timeShift->text().size() - 1);
        timeShift->setToolTip("Enter a time offset for depth data: [+/-]" + timeFormatExample);

        auto timeFormat = new QLineEdit;
        timeFormat->setText(userTimeFormat);
        timeFormat->setPlaceholderText("Enter time format");
        timeFormat->setToolTip("Enter time format to unpack values from time column"
                               "\n    Example:"
                               "\n        hh:mm:ss.zzz            \t- 22:34:45.123"
                               "\n        yyyyMMddhhmmss          \t- 20170506223445"
                               "\n        yyyy-MM-dd hh:mm:ss     \t- 2017-05-06 22:34:45"
                               "\n        dd.MM.yyyy hh:mm:ss     \t- 06.05.2017 22:34:45"
                               "\n        ddd dd MM yyyy hh:mm:ss \t- Сб 06 05 2017 22:34:45"
                               );

        auto formatExample = new QLineEdit;
        formatExample->setReadOnly(true);
        formatExample->setEnabled(false);
        formatExample->setToolTip("Unpacked time value by time format: " + timeFormatExample);

        //----------------------------------------------------------------------------------------------------------------
        auto depthCombo = new QComboBox;
        depthCombo->addItems(list);
        depthCombo->setToolTip("Select column in file, where depth data is located");
        static int depthComboIndex = depthCombo->count() > 1 ? 1 : 0;

        auto depthExample = new QLineEdit;
        depthExample->setReadOnly(true);
        depthExample->setEnabled(false);
        depthExample->setToolTip("Depth column first value");

        //----------------------------------------------------------------------------------------------------------------
        auto buttons = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);

        //----------------------------------------------------------------------------------------------------------------
        auto showError = [] (QWidget *w)
        {
            for (auto i=0; i<6;)
            {
                QTimer().singleShot(i++ * 100, [=] () { w->setStyleSheet("background-color:red;"); });
                QTimer().singleShot(i++ * 100, [=] () { w->setStyleSheet("background-color:white;"); });
            }
        };

        QString errStr = "Unknown!";

        // проверка введенного формата
        auto timeCheck = [=] (const QString &text, bool showErr = true)
        {
            auto dt = QDateTime::fromString(listExample.value(timeCombo->currentIndex(), errStr), text);
            if (dt.isValid())
            {
                if (dt.date().year() < QDate(1970, 01, 01).year())
                    dt.setDate(QDate::currentDate());

                int sign = timeShift->text().at(0) == '+' ? 1 :-1;

                QRegExp rx("(\\d+)");
                QList<int> list;
                int pos = 0;
                while ((pos = rx.indexIn(timeShift->text(), pos)) != -1)
                {
                    list << rx.cap(1).toInt();
                    pos += rx.matchedLength();
                }

                auto newDt = dt;

                newDt = newDt.addYears (sign * list[0]);
                newDt = newDt.addMonths(sign * list[1]);
                newDt = newDt.addDays  (sign * list[2]);
                newDt = newDt.addSecs  (sign * (list[3] * 60 * 60 + list[4] * 60 + list[5]));

                *dtShift = dt.secsTo(newDt);

                formatExample->setText(newDt.toString(timeFormatExample));
                userTimeFormat = text;
                return true;
            }
            if (showErr)
            {
                formatExample->setText("Can't parse time!");
                showError(formatExample);
            }
            return false;
        };
        // реакция на выбор нового столбца времени
        connect(timeCombo, QOverload<int>::of(&QComboBox::currentIndexChanged), this, [=] (int index)
        {
            bool timeOk = false;
            foreach (auto timeStr, timeList)
                if (!timeOk)
                    timeOk = timeCheck(timeStr, false);
                else
                    break;
            if (timeOk)
                timeFormat->setText(userTimeFormat);
            else
            {
                timeCheck(userTimeFormat);
                showError(formatExample);
            }

            timeComboIndex = index;
            timeExample->setText(listExample.value(index, errStr));
            if (timeExample->text() == errStr)
                showError(timeExample);
            timeFormat->textChanged(timeFormat->text());
        });
        //
        auto checkSift = [=] (auto step)
        {
            static int sign = 1;
            if (timeShift->text().at(0) == '+')
                sign = 1;
            else
                sign = -1;

            QRegExp rx("(\\d+)");
            QList<int> list;
            int pos = 0;
            while ((pos = rx.indexIn(timeShift->text(), pos)) != -1)
            {
                list << rx.cap(1).toInt();
                pos += rx.matchedLength();
            }

            if (!list[0] && !list[1] && !list[2] && !list[3] && !list[4] && !list[5])
                if ((step < 0 && sign > 0) || (step > 0 && sign < 0))
                    sign = -sign;
            if (sign < 0)
                step = -step;

            auto changeNext = false;
            pos = timeShift->cursorPosition();

            auto chenge = [=] (auto *changeNext, auto *val, auto step, auto max)
            {
                if ((*val <= 0 && step < 0) || (*val >= max && step > 0))
                    *changeNext = true;
                else
                    *changeNext = false;

                if (*val <= 0 && step < 0)
                    *val = max;
                else if (*val >= max && step > 0)
                    *val = 0;
                else
                    *val += step;
            };

            if ((pos > 17))                           { chenge(&changeNext, &list[5], step, 59); }
            if ((pos > 14 && pos < 17) || changeNext) { chenge(&changeNext, &list[4], step, 59); }
            if ((pos > 11 && pos < 14) || changeNext) { chenge(&changeNext, &list[3], step, 23); }
            if ((pos > 8 && pos < 11)  || changeNext) { chenge(&changeNext, &list[2], step, 31); }
            if ((pos > 5 && pos < 8)   || changeNext) { chenge(&changeNext, &list[1], step, 12); }
            if ((pos < 5)              || changeNext) { chenge(&changeNext, &list[0], step, 9999); }
            timeShift->setText(userTimeShift = QString("%1%2-%3-%4 %5:%6:%7")
                                .arg(sign > 0 ? '+' : '-')
                                .arg(list[0], 4, 10, QChar('0'))
                                .arg(list[1], 2, 10, QChar('0'))
                                .arg(list[2], 2, 10, QChar('0'))
                                .arg(list[3], 2, 10, QChar('0'))
                                .arg(list[4], 2, 10, QChar('0'))
                                .arg(list[5], 2, 10, QChar('0'))
                               );
            timeShift->setCursorPosition(pos);
            timeFormat->textChanged(timeFormat->text());
        };
        connect(timeShift, &QLineEdit::textEdited, this, [=] (const QString &)
        {
            checkSift(0);
        });
        connect(timeShift, &QLineEditMy::mouseWheelEvent, this, [=] (QWheelEvent* event)
        {
            checkSift(event->delta() < 0 ? -1 : 1);
        });
        // реакция на ввод формата времени
        connect(timeFormat, &QLineEdit::textChanged, this, [=] (const QString &text)
        {
            buttons->button(QDialogButtonBox::Ok)->setEnabled(timeCheck(text));
        });
        // реакция на выбор нового столбца глубины
        connect(depthCombo, QOverload<int>::of(&QComboBox::currentIndexChanged), this, [=] (int index)
        {
            depthComboIndex = index;
            depthExample->setText(listExample.value(index, errStr));
            if (depthExample->text() == errStr)
                showError(depthExample);
//            depthExample->setText(QString("Column %1: ").arg(index + 1) + depthExample->text());
        });
        // кнопки
        connect(buttons, SIGNAL(accepted()), dialog, SLOT(accept()));
        connect(buttons, SIGNAL(rejected()), dialog, SLOT(reject()));

        //
        timeCombo->currentIndexChanged(timeComboIndex);
        timeCombo->setCurrentIndex(timeComboIndex);

        depthCombo->currentIndexChanged(depthComboIndex);
        depthCombo->setCurrentIndex(depthComboIndex);

        auto row = 0;
        auto col = 0;
        auto layout = new QGridLayout;
        layout->addWidget(new QLabel("Time column:")    , row, col++);
        layout->addWidget(timeCombo                     , row, col++);
        layout->addWidget(timeExample                   , row, col++);
        row++; col = 0;
        layout->addWidget(new QLabel("Time shift:")     , row, col++);
        layout->addWidget(new QLabel()                  , row, col++);
        layout->addWidget(timeShift                     , row, col++);
        row++; col = 0;
        layout->addWidget(new QLabel("Time format:")    , row, col++);
        layout->addWidget(timeFormat                    , row, col++);
        layout->addWidget(formatExample                 , row, col++);
        row++; col = 0;
        layout->addWidget(new QLabel("Depth column:")   , row, col++);
        layout->addWidget(depthCombo                    , row, col++);
        layout->addWidget(depthExample                  , row, col++);

        auto mainLayout = new QVBoxLayout;
        mainLayout->addLayout(layout);
        mainLayout->addWidget(buttons);
        dialog->setLayout(mainLayout);
        dialog->setModal(true);
        if (dialog->exec() == QDialog::Rejected)
            return;

        //----------------------------------------------------------------------------------------------------------------
        // чтене данных и формирование глубины
        file.seek(0);       // начинаем с начала
        file.readLine();    // попускаем заголовок

        QProgressDialog progress("Load data...", "Cancel", 0, static_cast<int>(file.size()), this);
        progress.setWindowModality(Qt::ApplicationModal);
        progress.setValue(0);
        progress.show();

        mainTime = wellTime;
        graphData vecDepth;
        bool ok;

        // парcим файл
        while (!file.atEnd())
        {
            if (!updateProbress(&progress, static_cast<int>(file.pos())))
                break;

            list = readListFromFile(&file);

            auto dt = QDateTime::fromString(list.value(timeCombo->currentIndex(), errStr), timeFormat->text());
            auto depth = list.value(depthCombo->currentIndex(), errStr).toDouble(&ok);

            if (dt.date().year() < QDate(1970, 01, 01).year())
                dt.setDate(QDate::currentDate());

            if (dt.isValid() && ok)
            {
                dt = dt.addSecs(*dtShift);
                mainTime.append(dt.toTime_t());
                vecDepth.append(QCPGraphData(dt.toTime_t(), depth));
            }
        }
        file.close();
        mainTime.squeeze();
        vecDepth.squeeze();

        if (auto graph = ui->customPlot->findChild<QCPGraph *>("MainTime"))
            graph->data()->set(makeGraphData(&mainTime, 0, false));
        if (auto graph = ui->customPlot->findChild<QCPGraph *>("DEPT, m"))
            graph->data()->set(vecDepth);
        haveDepth = true;

        // обновление
        deselectAll();
        replot();
    }
    else
        QMessageBox::critical(nullptr, "Error!", QString("Error open file:\n%1").arg(fileName), QMessageBox::Ok, QMessageBox::Ok);
}

void Diagrams::exportData()
{
    exportDat();
}

void Diagrams::exportDat(QString name, QString filter)
{
    enum
    {
        FILE_LAS_DEPTH,
        FILE_LAS_TIME,
        FILE_CSV_DEPTH,
        FILE_CSV_TIME,
    };

    auto fileFilters = QStringList()
            << "LAS file (*.las)"
            << "LAS file by time (*.las)"
            << "CSV file (*.csv)"
            << "CSV file by time (*.csv)"
               ;

    auto filters = fileFilters;
    if (!haveDepth)
    {
        filters.removeAll("LAS file (*.las)");
        filters.removeAll("CSV file (*.csv)");
    }

    enum
    {
        sizeHead = 25,
        sizeTime = 20,
        sizeData = 15,
    };

    auto fileName = name;
    if (name.isEmpty() || filter.isEmpty())
    {
        fileName = QFileDialog::getSaveFileName(this, "Save File", this->property("fileName").toString().remove(".dat"), filters.join(";;"), &filter);
        if (fileName.isEmpty())
            return;
    }

    auto exportMode = fileFilters.indexOf(filter);

    QFile file(fileName);
    if(!file.open(QIODevice::WriteOnly))
    {
        QMessageBox::critical(nullptr, "Error!", tr("Error save file:\n%1").arg(file.fileName()), QMessageBox::Ok, QMessageBox::Ok);
        return;
    }

    QTextStream out(&file);
    out.setCodec(QTextCodec::codecForName("Windows-1251"));

    QProgressDialog progress("Save file", "Cancel", 0, 0, this);
    progress.setWindowModality(Qt::ApplicationModal);
    progress.setValue(0);
    progress.show();

    auto tracer = new QCPItemTracer(ui->customPlot);
    tracer->setVisible(false);

    double errVal = -999.25;
    QString term = "\r\n";

    auto strForm = [=] (QString text, int length = sizeHead, bool left = true)
    {
        if (left)
            return QString(text).leftJustified(length);
        return QString(text).rightJustified(length);
    };

    auto timeToStr = [=] (double time, QString format, int length = sizeHead, bool left = true)
    {
        if (left)
            return QDateTime::fromMSecsSinceEpoch(static_cast<int64_t>(time)).toString(format).leftJustified(length);
        return QDateTime::fromMSecsSinceEpoch(static_cast<int64_t>(time)).toString(format).rightJustified(length);
    };

    //---------------------------------------------------------------------------------------------------------------
    // сохранение данных
    QVector<double> lasDepthTime;
    graphData vecLasDepth;

    progress.setMinimum(0);
    progress.setMaximum(mainTime.size());
    if (exportMode == FILE_CSV_DEPTH || exportMode == FILE_LAS_DEPTH)
    {
        auto startTag = ui->customPlot->findChild<AxisTagD *>("StartTag");
        auto stopTag = ui->customPlot->findChild<AxisTagD *>("StopTag");

        if (!startTag || !stopTag)
            return;

        if (!startTag->isVisible || !stopTag->isVisible)
        {
            QMessageBox::information(this, "Attention!",
                                     "Select an interval for export.\n"
                                     "To do this, put the start and stop marks on the depth tablet.\n"
                                     "Having pressed the right mouse button,\n"
                                     "select the starting position and fix it by pressing\n"
                                     "the left mouse button (while keeping the right key pressed),\n"
                                     "also select the stop position.\n"
                                     "Resetting marks is carried out by clicking the left mouse button.");
            return;
        }

        auto start = startTag->position;
        auto stop = stopTag->position;

        if (auto graph = ui->customPlot->findChild<QCPGraph *>("DEPT, m"))
        {
            auto getDepth = [=] (auto val)
            {
                tracer->setGraphKey(val);
                tracer->setClipAxisRect(graph->keyAxis()->axisRect());
                tracer->setGraph(graph);
                return tracer->position->coords().y();
            };

            auto startDepth = getDepth(start);
            auto stopDepth = getDepth(stop);

            if (stop < start)
            {
                double value = startDepth < stopDepth ? -1.e9 : +1.e9;
                for (auto i=mainTime.size()-1; i>=0; i--)
                {
                    if (mainTime.at(i) < stop)
                        break;
                    if (mainTime.at(i) <= start)
                    {
                        auto val = getDepth(mainTime.at(i));
                        if ((startDepth < stopDepth && val > value) || (startDepth > stopDepth && val < value))
                        {
                            lasDepthTime.append(tracer->position->coords().x());
                            vecLasDepth.append(QCPGraphData(lasDepthTime.last(), value = val));
                        }
                    }
                }
            }
            else
            {
                double value = startDepth < stopDepth ? -1.e9 : +1.e9;
                for (auto i=0; i<mainTime.size(); i++)
                {
                    if (mainTime.at(i) > stop)
                        break;
                    if (mainTime.at(i) >= start)
                    {
                        auto val = getDepth(mainTime.at(i));
                        if ((startDepth < stopDepth && val > value) || (startDepth > stopDepth && val < value))
                        {
                            lasDepthTime.append(tracer->position->coords().x());
                            vecLasDepth.append(QCPGraphData(lasDepthTime.last(), value = val));
                        }
                    }
                }
            }
        }
        lasDepthTime.squeeze();
        vecLasDepth.squeeze();
        if (auto graph = ui->customPlot->findChild<QCPGraph *>("LAS DEPT, m"))
            graph->data()->set(vecLasDepth);

        progress.setMaximum(lasDepthTime.size());

        // обновление
        deselectAll();
        replot();
    }

    auto check = [] (auto graph, auto exportMode)
    {
        if (graph->property("Visible").toBool())
        {
            if (exportMode == FILE_CSV_TIME || exportMode == FILE_LAS_TIME)
                return !graph->name().contains("DEPT, m");
            else if (exportMode == FILE_CSV_DEPTH || exportMode == FILE_LAS_DEPTH)
                return !graph->name().contains("LAS DEPT, m");
        }
        return false;
    };

    if (exportMode == FILE_CSV_TIME || exportMode == FILE_CSV_DEPTH)
    {
        if (exportMode == FILE_CSV_TIME)
            out << "Time;";
        foreach (auto rect, rectList)
            foreach (auto graph, rect->graphs())
                if (check(graph, exportMode))
                    out << graph->name() + ";";
        out << term;
    }
    else
    {
        QString strS;
        QString strP;
        QString strH;

        if (exportMode == FILE_LAS_TIME)
        {
            strS += " " + strForm("STRT.S") + timeToStr(mainTime.first() * 1000, defLasTimeFormat) + ": START TIME" + term;
            strS += " " + strForm("STOP.S") + timeToStr(mainTime.last() * 1000,  defLasTimeFormat) + ": STOP TIME"  + term;
            strS += " " + strForm("STEP.S") + strForm("0.000")                                     + ": STEP TIME"  + term;
        }
        else
        {
            strS += " " + strForm("STRT.M") + strForm(QString("%1").arg(vecLasDepth.value(0, QCPGraphData(0, 0)).value, 0, 'f', 4))                    + ": START" + term;
            strS += " " + strForm("STOP.M") + strForm(QString("%1").arg(vecLasDepth.value(vecLasDepth.size()-1, QCPGraphData(0, 0)).value, 0, 'f', 4)) + ": STOP"  + term;
            strS += " " + strForm("STEP.M") + strForm("0.000")                                                                                         + ": STEP"  + term;
        }

        auto paramNum = 1;
        if (exportMode == FILE_LAS_TIME)
            strP += " " + strForm("ETIM") + strForm(".s") + QString(": %1").arg(paramNum++) + term;
        foreach (auto rect, rectList)
            foreach (auto graph, rect->graphs())
                if (check(graph, exportMode))
                    strP += " " + strForm(graph->name().split(", ").first().replace(' ', '_')) + strForm("." + graph->name().split(", ").last()) + QString(": %1").arg(paramNum++) + term;

        if (exportMode == FILE_LAS_TIME)
            strH += strForm("ETIM", sizeTime-2, false);
        foreach (auto rect, rectList)
            foreach (auto graph, rect->graphs())
                if (check(graph, exportMode))
                    strH += strForm(graph->name().split(", ").first().replace(' ', '_'), sizeData, false);
        if (exportMode == FILE_LAS_DEPTH)
            strH.remove(0, 2);

        const auto lasHeadDateFormat = "dd/MM/yyyy";
        const auto lasHeadTimeFormat = "hh-mm-ss";

        out << "#" + term
            << "~VERSION INFORMATION SECTION" + term
            << "VERS.   2.0  : LAS VERSION 2.0 BY MAKS-W EXPORT 1.0" + term
            << "WRAP.   NO   : ONE LINE PER DEPTH|TIME STEP" + term
            << "#" + term
            << "~WELL INFORMATION SECTION" + term
            << "#" + strForm("MNEM.UNIT") + strForm("DATA TYPE") + strForm("INFORMATION") + term
            << "#" + QString().fill('-', sizeHead - 1) + " " + QString().fill('-', sizeHead - 1) + " " + QString().fill('-', sizeHead - 1) + term
            << strS
            << " " + strForm("NULL.")     + strForm(QString("%1").arg(errVal, 0, 'f', 4))         + ": NULL VALUE"            + term
            << " " + strForm("COMP.")     + strForm("")                                           + ": COMPANY"               + term
            << " " + strForm("WELL.")     + strForm("")                                           + ": Well Name"             + term
            << " " + strForm("FLD.")      + strForm("")                                           + ": Field Name"            + term
            << " " + strForm("LOC.")      + strForm("")                                           + ": Location"              + term
            << " " + strForm("DATE.")     + timeToStr(mainTime.first() * 1000, lasHeadDateFormat) + ": Date of Start Logging" + term
            << " " + strForm("TIME.")     + timeToStr(mainTime.first() * 1000, lasHeadTimeFormat) + ": Time of Start Logging" + term
            << " " + strForm("DATO.")     + timeToStr(mainTime.last()  * 1000, lasHeadDateFormat) + ": Date of Stop Logging"  + term
            << " " + strForm("TIMO.")     + timeToStr(mainTime.last()  * 1000, lasHeadTimeFormat) + ": Time of Stop Logging"  + term
            << "#" + term
            << "~PARAMETER INFORMATION SECTION" + term
            << "#" + term
            << "~CURVE INFORMATION SECTION" + term
            << "#" + strForm("MNEM.UNIT API") + strForm("CODE") + strForm("CURVE DESCRIPTION") + term
            << "#" + QString().fill('-', sizeHead - 1) + " " + QString().fill('-', sizeHead - 1) + " " + QString().fill('-', sizeHead - 1) + term
            << strP
            << "#" + term
            << "~OTHER INFORMATION SECTION" + term
            << "#" + term
            << "~ASCII LOG DATA SECTION" + term
            << "# " + strH + term
            ;
    }

    for (auto i=progress.minimum(); i<progress.maximum(); i++)
    {
        if (!updateProbress(&progress, static_cast<int>(i)))
            break;

        if (exportMode == FILE_CSV_DEPTH || exportMode == FILE_LAS_DEPTH)
            tracer->setGraphKey(lasDepthTime.at(i));
        else
            tracer->setGraphKey(mainTime.at(i));

        int first = 0;
        foreach (auto rect, rectList)
        {
            tracer->setClipAxisRect(rect);
            foreach (auto graph, rect->graphs())
            {
                if (check(graph, exportMode))
                {
                    tracer->setGraph(graph);
                    if (!first++)
                    {
                        if (exportMode == FILE_CSV_TIME)
                            out << QDateTime::fromMSecsSinceEpoch(static_cast<int64_t>(tracer->position->coords().x() * 1000)).toString(defCsvTimeFormat) << ';';
                        else if (exportMode == FILE_LAS_TIME)
                            out << timeToStr(tracer->position->coords().x() * 1000, defLasTimeFormat, sizeTime, false);
                    }
                    if (exportMode == FILE_CSV_TIME || exportMode == FILE_CSV_DEPTH)
                        out << QString::number(tracer->position->coords().y(), 'f', 3) << ';';
                    else
                        out << strForm(QString::number(tracer->position->coords().y(), 'f', 3), sizeData, false);
                }
            }
        }
        out << term;
    }

    file.close();
}

void Diagrams::exportD()
{
    if (!haveDepth)
    {
        exportDat(this->property("fileName").toString().remove(".dat") + " export TIME.las", "LAS file by time (*.las)");
        exportDat(this->property("fileName").toString().remove(".dat") + " export TIME.csv", "CSV file by time (*.csv)");
        return;
    }
    exportDat(this->property("fileName").toString().remove(".dat") + " export TIME&DEPT.las", "LAS file by time (*.las)");
    exportDat(this->property("fileName").toString().remove(".dat") + " export TIME&DEPT.csv", "CSV file by time (*.csv)");
    exportDat(this->property("fileName").toString().remove(".dat") + " export DEPT.las", "LAS file (*.las)");
    exportDat(this->property("fileName").toString().remove(".dat") + " export DEPT.csv", "CSV file (*.csv)");
}

void Diagrams::keyPressEvent(QKeyEvent *event)
{
    switch (event->key())
    {
        case Qt::Key_PageUp:   ui->bar->triggerAction(QAbstractSlider::SliderPageStepSub);   break;
        case Qt::Key_PageDown: ui->bar->triggerAction(QAbstractSlider::SliderPageStepAdd);   break;
        case Qt::Key_Up:       ui->bar->triggerAction(QAbstractSlider::SliderSingleStepSub); break;
        case Qt::Key_Down:     ui->bar->triggerAction(QAbstractSlider::SliderSingleStepAdd); break;
        case Qt::Key_Home:     ui->bar->triggerAction(QAbstractSlider::SliderToMinimum);     break;
        case Qt::Key_End:      ui->bar->triggerAction(QAbstractSlider::SliderToMaximum);     break;
        default:               QWidget::keyPressEvent(event); break;
    }
}
