#include <QtWidgets>

#include "qcustomplot.h"

#ifndef AXISTAGD_H
#define AXISTAGD_H

class AxisTagD : public QObject
{
    Q_OBJECT

public:
    explicit AxisTagD(QCPAxis *parentAxis, QString objectName, QCPItemTracer::TracerStyle style = QCPItemTracer::tsNone, bool onlyTracer = false);
    ~AxisTagD() override;

    void setBrush(const QBrush &brush);
    void setText(const QString &text);
    void setVisible(bool visible);

    QBrush brush() const { return mLabel->brush(); }
    QString text() const { return mLabel->text(); }

    void setPosition(double value);
    bool isVisible;
    double position;

    QCPAxis *mAxis;

    QPointer<QCPItemTracer> mDummyTracer;
    QPointer<QCPItemLine> mArrow;
    QPointer<QCPItemText> mLabel;

protected:

};

#endif // AXISTAGD_H

#ifndef DIAGRAMS_H
#define DIAGRAMS_H

using graphData = QVector<QCPGraphData>;
typedef QMap<double, graphData> oscData;

namespace Ui {
class Diagrams;
}

class Diagrams : public QWidget
{
    Q_OBJECT

public:
    explicit Diagrams(QWidget *parent = nullptr);

    QCPAxisRect *addRect(QCPLayoutGrid *layoutGrid = nullptr, bool visible = true);
    void addGraphPoints(const QString &name, const QVector<QCPGraphData>& data);
    QCPGraph *addGraph(QCPAxisRect *rect, const QVector<QCPGraphData>& data = QVector<QCPGraphData>(), const QString& name = " ", const QColor& color = Qt::blue, bool visible = false, bool last = false);
    QCPGraph *addGraphWav(const QString& name = " ", const QString& nameX = "", const QString& nameY = "", const QColor& color = Qt::blue);

    graphData makeGraphData(QVector<double> *valX, double valY, bool alreadySorted = true);

    void updateDiagrams();

    QList<QCPAxisRect *> rectList;
    QList<QCPAxisRect *> rectWavList;

private:
    Ui::Diagrams *ui;
    QCPLayoutGrid *QCPmainLayout;
    QCPLayoutGrid *QCPsubLayout;
    QList<AxisTagD *> rectTagList;
    QList<AxisTagD *> graphTagList;
    QList<QCPItemTracer *> graphTracerList;
    QCPRange maxRange;
    QPoint mMousePressPos;
    QCPAxis *selectedAxis{};
    Qt::MouseButtons buttonsPressed;

    QVector<double> wellTime;
    QVector<double> mainTime;

    bool haveDepth;

    void clearAll();
    void deselectAll();
    QCPRange getAxisFullRange(QCPAxis *axis);
    void axisUpdateRange(QCPAxis *axis);
    void replot();
    QCPAxis *mainAxis();
    QPen setInvisibleAxis(QCPAxis *axis);
    void rescaleAxis(QCPAxis *axis);
    QCPRange axisChanged(const QCPRange &newRange);
    void scrollUpdate(QCPRange maxRange, QCPRange range);

    QMap<QString, oscData *> oscMap;

    QElapsedTimer *timerMouseMove;

    QElapsedTimer wakeTime;
    bool updateProbress(QProgressDialog *progress, int val)
    {
        if (progress->wasCanceled())
            return false;
        if (wakeTime.elapsed() > 200)
        {
            progress->setValue(val);
            wakeTime.restart();
            QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents, 5);
        }
        return true;
    }

public slots:
    void setAutoScale(QCPAxis *axis, bool checked);
    void setAxisScaleType(QCPAxis *axis, QCPAxis::ScaleType type);

private slots:
    void scrollBarChanged(int value = 0);

    void mouseWheel(QWheelEvent *event);
    void mouseMove(QMouseEvent *event);
    void mousePress(QMouseEvent *event);
    void mouseRelease(QMouseEvent *event);
    void selectionChanged();

    QList<QAction *> newAxisActions(QCPAxis *axis);
    void contextMenuRequest(QPoint pos);
    void screenshotGraphs();
//    void parseMemFile();
    void addDepth();
    void exportData();
    void exportDat(QString name = "", QString filter = "");
    void exportD();
    void clearGraph(const QString &name);
    void clearAllGraph();

protected:
    void keyPressEvent(QKeyEvent *event) Q_DECL_OVERRIDE;

    void delay(int millisecondsToWait)
    {
        QTime dieTime = QTime::currentTime().addMSecs(millisecondsToWait);
        while (QTime::currentTime() < dieTime)
            QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
    }

};

#endif // DIAGRAMS_H

//! \brief Класс для отлова показа всплывающего окна
class QLineEditMy : public QLineEdit
{
    Q_OBJECT

public:

    void wheelEvent(QWheelEvent* event)
    {
        mouseWheelEvent(event);
    }

signals:
    void mouseWheelEvent(QWheelEvent* event);

};
